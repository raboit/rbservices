<?php

Yii::import('common.models._base.BaseVenta');

class Venta extends BaseVenta
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function calcularTotal(){
            $total=0;
            foreach ($this->stocks as $detalle) {
                $total=$total+$detalle->precio_venta;                
            }
            return $total;
        }
        
        
        public function getMenu(){
            if($this->estado=="NUEVA"){            
                echo TbHtml::linkButton('Agregar Detalle', array(
                    'icon'=>'shopping-cart white',
                    'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                    'url' => Yii::app()->createUrl('venta/crearDetalleVenta',array('id'=>$this->id)),
                    ))." ";            
                echo TbHtml::linkButton('Pagar', array(
                  'icon'=>'ok white',
                  'color' => TbHtml::BUTTON_COLOR_SUCCESS,
                  'onclick'=>'js:ingresarPago()',
                  'data-toggle' => 'modal',
                  'data-target' => '#myModal',
                  ))." ";  
            }
            
            if($this->estado!="ANULADA"){
                echo TbHtml::linkButton('Anular venta', array(
                    'icon'=>'remove white',
                    'color' => TbHtml::BUTTON_COLOR_DANGER,
                    'url' => Yii::app()->createUrl('venta/anular',array('id'=>$this->id)),
                    ))." ";   
            }
        }
        public function gridStockVenta(){
            
            if($this->estado=="NUEVA")
                return array('/venta/crear_detalle/formulario_NUEVA', array(
                    'model' => $this->stocks,
                    'buttons' => 'create'));
            else
                return array('/venta/crear_detalle/formulario_OTRO', array(
                    'model' => $this->stocks,
                    'buttons' => 'create'));
            
        }
        
        public static function getColumns(){
            return array(
		'id',
		'fecha',
		'total',
		'estado',
//		array(
//				'name'=>'user_id',
//				'value'=>'GxHtml::valueEx($data->user)',
//				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
//				),
                array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'viewButtonUrl'=>'Yii::app()->createUrl("/venta/ver", array("id"=>$data->id))',
                        'updateButtonUrl'=>'Yii::app()->createUrl("/venta/actualizar", array("id"=>$data->id))',
                        'deleteButtonUrl'=>'Yii::app()->createUrl("/venta/borrar", array("id"=>$data->id))',
                  ),                
	);
        }
        
        public function ventas() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('fecha', $this->fecha, true);
		$criteria->compare('total', $this->total);
		$criteria->compare('estado', $this->estado, true);
		$criteria->compare('user_id', Yii::app()->user->getId());
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}