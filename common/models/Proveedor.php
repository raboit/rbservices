<?php

Yii::import('common.models._base.BaseProveedor');

class Proveedor extends BaseProveedor
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public static function getColumns(){
            return array(
		'id',
		'nombre',
		'telefono',
		'email',
		'direccion',
//		array(
//				'name'=>'user_id',
//				'value'=>'GxHtml::valueEx($data->user)',
//				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
//				),
                array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'viewButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->createUrl("ver"), array("id"=>$data->id))',
                        'updateButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->createUrl("actualizar"), array("id"=>$data->id))',
                        'deleteButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->createUrl("borrar"), array("id"=>$data->id))',
                  ),                
                );
        }
        
        public function proveedores() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nombre', $this->nombre, true);
		$criteria->compare('telefono', $this->telefono, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('direccion', $this->direccion, true);
		$criteria->compare('user_id', Yii::app()->user->getId());
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}