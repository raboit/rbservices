<?php

Yii::import('common.models._base.BaseStock');

class Stock extends BaseStock
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function getEtiquetaEstado(){
            if($this->estado=="ANULADO")
                return TbHtml::labelTb($this->estado, array("color" => TbHtml::LABEL_COLOR_IMPORTANT));
            elseif($this->estado=="NUEVO")
                return TbHtml::labelTb($this->estado, array("color" => TbHtml::LABEL_COLOR_SUCCESS));
            else
                return TbHtml::labelTb($this->estado, array("color" => TbHtml::LABEL_COLOR_INFO));
        }
        
        public static function getColumns($attributes=array()){
            return array(
		(isset($attributes['id']))?array_merge(array('name'=>'id'),$attributes['id']):'id',
                (isset($attributes['codigo_barra']))?array_merge(array('name'=>'codigo_barra'),$attributes['codigo_barra']):'codigo_barra',
                (isset($attributes['fecha_creacion']))?array_merge(array('name'=>'fecha_creacion'),$attributes['fecha_creacion']):'fecha_creacion',
                (isset($attributes['compra_id']))?array_merge(array('name'=>'compra_id','filter'=>GxHtml::listDataEx(Compra::model()->findAllAttributes(null, true))),$attributes['compra_id']):array('name'=>'compra_id','value'=>'GxHtml::valueEx($data->compra)','filter'=>GxHtml::listDataEx(Compra::model()->findAllAttributes(null, true)),),
                
                (isset($attributes['estado']))?array_merge(array('name'=>'estado','filter'=>array('NUEVO'=>'NUEVO','ANULADO'=>'ANULADO','VENDIDO'=>'VENDIDO'),'htmlOptions'=>array('style'=>'width: 10%')),$attributes['estado']):array('name'=>'estado','filter'=>array('NUEVO'=>'NUEVO','ANULADO'=>'ANULADO','VENDIDO'=>'VENDIDO')),
                
		
//		array(
//				'name'=>'venta_id',
//				'value'=>'GxHtml::valueEx($data->venta)',
//				'filter'=>GxHtml::listDataEx(Venta::model()->findAllAttributes(null, true)),
//				),
//               array(
//				'name'=>'user_id',
//				'value'=>'GxHtml::valueEx($data->user)',
//				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
//				),
                (isset($attributes['buttons']))?array_merge(array('class'=>'bootstrap.widgets.TbButtonColumn'),$attributes['buttons']):array('class'=>'bootstrap.widgets.TbButtonColumn','viewButtonUrl'=>'Yii::app()->createUrl("/stock/ver", array("id"=>$data->id))','updateButtonUrl'=>'Yii::app()->createUrl("/stock/actualizar", array("id"=>$data->id))','deleteButtonUrl'=>'Yii::app()->createUrl("/stock/borrar", array("id"=>$data->id))',),
                              
                );
        }
    
        public function stocks() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('codigo_barra', $this->codigo_barra);
		$criteria->compare('fecha_creacion', $this->fecha_creacion, true);
		$criteria->compare('estado', $this->estado, true);
		$criteria->compare('compra_id', $this->compra_id);
		$criteria->compare('venta_id', $this->venta_id);
		$criteria->compare('user_id', Yii::app()->user->getId());
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>array(
                            'pageSize'=>200,
                        ),
		));
	}
}