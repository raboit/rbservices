<?php

Yii::import('common.models._base.BaseCompra');

class Compra extends BaseCompra
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public static function getColumns(){
            return array(
		'id',
		'fecha',
		'precio',
		'cantidad_productos',
		'estado',
		array(
				'name'=>'producto_id',
				'value'=>'GxHtml::valueEx($data->producto)',
				'filter'=>GxHtml::listDataEx(Producto::model()->findAllAttributes(null, true)),
				),
		/*
		array(
				'name'=>'proveedor_id',
				'value'=>'GxHtml::valueEx($data->proveedor)',
				'filter'=>GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
				),
		*/
                array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'viewButtonUrl'=>'Yii::app()->createUrl("/compra/ver", array("id"=>$data->id))',
                        'updateButtonUrl'=>'Yii::app()->createUrl("/compra/actualizar", array("id"=>$data->id))',
                        'deleteButtonUrl'=>'Yii::app()->createUrl("/compra/borrar", array("id"=>$data->id))',
                  ),                
                );
        }
        
        public function compras() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
                $criteria->compare('fecha', Yii::app()->format->FormatoFechaBD($this->fecha), true);
                if(!empty($this->fecha_inicio) && !empty($this->fecha_termino))   
                        $criteria->addBetweenCondition('fecha', Yii::app()->format->FormatoFechaBD($this->fecha_inicio), Yii::app()->format->FormatoFechaBD($this->fecha_termino));
		$criteria->compare('precio', $this->precio);
		$criteria->compare('cantidad_productos', $this->cantidad_productos);
		$criteria->compare('estado', $this->estado, true);
		$criteria->compare('producto_id', $this->producto_id);
		$criteria->compare('proveedor_id', $this->proveedor_id);
		$criteria->compare('user_id', Yii::app()->user->getId());
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}