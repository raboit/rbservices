<?php

Yii::import('common.models._base.BaseProducto');

class Producto extends BaseProducto
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public static function getColumns(){
            return array(
		'id',
		'nombre',
		'precio',
//		array(
//				'name'=>'user_id',
//				'value'=>'GxHtml::valueEx($data->user)',
//				'filter'=>GxHtml::listDataEx(User::model()->findAllAttributes(null, true)),
//				),
                array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'viewButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->createUrl("ver"), array("id"=>$data->id))',
                        'updateButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->createUrl("actualizar"), array("id"=>$data->id))',
                        'deleteButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->createUrl("borrar"), array("id"=>$data->id))',
                  ),                
                );
        }
        
        public function productos() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nombre', $this->nombre, true);
		$criteria->compare('precio', $this->precio);
		$criteria->compare('user_id', Yii::app()->user->getId());
                
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}