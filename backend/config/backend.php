<?php
/**
 *
 * backend.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'backend');

// web application configuration
return array(
	'name' => 'BackEnd',
	'basePath' => realPath(__DIR__ . '/..'),
	// path aliases
	'aliases' => array(
		'bootstrap' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiistrap',
		'yiiwheels' =>  dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiiwheels'
	),      
        
	// application behaviors
	'behaviors' => array(),

	// controllers mappings
	'controllerMap' => array(),

        'import'=>array(
		'backend.modules.rights.*',
                'backend.modules.rights.components.*',
                'backend.components.*',
                'backend.models.*',
	),
	// application modules
	'modules' => array(
            'rights'=>array(
                        'superuserName'=>'Administrador',
                        'authenticatedName'=>'Usuario',
                        'userIdColumn'=>'id',
                        'userNameColumn'=>'username',
                        'enableBizRule'=>true,
                        'enableBizRuleData'=>false,
                        'displayDescription'=>true,
                        'flashSuccessKey'=>'RightsSuccess',
                        'flashErrorKey'=>'RightsError',
                        'baseUrl'=>'/rights',
                        'appLayout'=>'//layouts/main',
                        'install'=>false,
                        'debug'=>false,
                ),
        ),

	// application components
	'components' => array(
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),          

		'clientScript' => array(
			'scriptMap' => array(
			),
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,

			'rules' => array(
				// default rules
				'<controller:\w+>/<id:\d+>'=>'<controller>/ver',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'user'=>array(
                        // enable cookie-based authentication
                        'allowAutoLogin'=>true,
                        'guestName'=>'Invitado',
                        'class'=>'RWebUser',
                ),
                'authManager'=>array(
                        'class'=>'RDbAuthManager',
                        'defaultRoles'=>array('Invitado', 'Usuario'),
                ),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
            
	),
);