<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=>'Ingresar', 'url'=>array('/site/login')),
            ),
        ),
    ),
)); ?>