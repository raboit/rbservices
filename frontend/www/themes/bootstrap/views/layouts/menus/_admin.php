
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'collapse'=>true,
    #'color'=>  TbHtml::NAVBAR_COLOR_INVERSE,
    'htmlOptions'=>array('id'=>'navigation'),
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'items'=>array(
                array('label'=>'Inicio', 'url'=>array('/site/index')),
                
                array('label'=> 'Venta', 'items'=>array(
                    array('label'=>'Nueva Venta', 'url'=>array('/venta/crear')),
                    TbHtml::menuDivider(),
                    array('label'=>'Administrar Ventas', 'url'=>array('/venta/administrar')),
                )),
                
                array('label'=> 'Inventario', 'items'=>array(
                    array('label'=>'Inventario', 'url'=>array('/stock/administrar')),
                    TbHtml::menuDivider(),
                    array('label'=>'Nueva Compra', 'url'=>array('/compra/crear')),
                )),
                
                array('label'=> 'Mantenedor', 'items'=>array(
                    array('label'=>'Proveedores', 'url'=>array('/proveedor/administrar')),
                    TbHtml::menuDivider(),
                    array('label'=>'Productos', 'url'=>array('/producto/administrar')),
                )),
                
            ),
        ),
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                array('label'=> Yii::app()->user->name, 'items'=>array(
                    array('label'=>'Salir', 'url'=>array('/site/logout')),
                    TbHtml::menuDivider(),   
                    array('label'=>'Usuarios', 'url'=>array('/user/index')),
                    array('label'=>'Roles', 'url'=>array('/rights/')),
                    TbHtml::menuDivider(),
                    array('label'=>'Cambiar Contraseña', 'url'=>array('/user/changePassword/', "id"=>Yii::app()->user->id)),
                )),
            ),
        ),
    ),
)); ?>
