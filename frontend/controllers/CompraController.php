<?php

class CompraController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('Compra');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        /*
         * Nombre:
         * VER COMPRA
         * Descripción:
         * FUNCION PARA VER UNA COMPRA Y AGREGAR STOCK A ESTA
         * Fecha:
         * 01/11/2013
         * 
         */
	public function actionVer($id) {
            $model_compra = $this->loadModel($id,'Compra');
            $model_stock= new Stock;
            
            if(isset($_POST['codigo_barra'])):
                $model_stock->codigo_barra=$_POST['codigo_barra'];
                $model_stock->compra_id=$id;
                $model_stock->producto_id=$model_compra->producto_id;
                $model_stock->fecha_creacion=date("Y-m-d H:i:s");
                $model_stock->user_id=Yii::app()->user->getId();
                $model_stock->estado="NUEVO";
                if($model_stock->validate()){
                    $model_stock->save(true);
                    Yii::app()->getUser()->setFlash('success','PRODUCTO INGRESADO EXITASAMENTE ');
                           
                }else{
                    Yii::app()->getUser()->setFlash('error','EL PRODUCTO YA SE ENCUENTRA REGISTRADO ');
                }
                
            endif;
            
		$this->render('ver', array(
			'model' => $model_compra,
		));
	}

	public function actionCrear() {
		$model = new Compra;
                $model->fecha=$model->fecha=date("Y-m-d");

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);
                        $model->user_id=Yii::app()->user->id;
			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Compra');

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Compra')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Compra('search');
		$model->unsetAttributes();

		if (isset($_GET['Compra'])){
			$model->setAttributes($_GET['Compra']);
                }

                $session['Compra_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Compra_model_search']))
               {
                $model = $session['Compra_model_search'];
                $model = Compra::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Compra::model()->findAll();
             $this->toExcel($model, array('id', 'fecha', 'precio', 'cantidad_productos', 'estado', 'producto', 'proveedor', 'user'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Compra_model_search']))
               {
                $model = $session['Compra_model_search'];
                $model = Compra::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Compra::model()->findAll();
             $this->toExcel($model, array('id', 'fecha', 'precio', 'cantidad_productos', 'estado', 'producto', 'proveedor', 'user'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

}