<?php

class VentaController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('Venta');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
            $model_venta=  $this->loadModel($id, 'Venta');
                if(isset($_POST['agregar'])){
                    if(isset($_POST['codigo_barra'])){
                        
                        $model_stock = Stock::model()->findByAttributes(array('estado'=>'NUEVO', 'codigo_barra'=>$_POST['codigo_barra']));
                        
                        if(isset($model_stock)){
                          $model_stock->venta_id =  $model_venta->id;
                          $model_stock->precio_venta = $model_stock->producto->precio;
                          
                          if($model_stock->validate())
                              $model_stock->save(true);
                          
                          Yii::app()->getUser()->setFlash('success','<i class="icon-ok-sign"></i> Producto agregado exitósamente!');
                        }else{
                            Yii::app()->getUser()->setFlash('error','Debe ingresar un producto <strong>disponible</strong> en el inventario.');
                        }  
                                                
                        
                    }
                }elseif(isset($_POST['terminar'])){                    
                        //$model_venta->total=$model_venta->calcularTotal();
                        $model_venta->estado="FINALIZADA";
                        if($model_venta->validate()){
                            $model_venta->save(false);
                            $this->redirect(array('ver', 'id' => $model_venta->id));  
                        }else
                            Yii::app()->getUser()->setFlash('error','Debe ingresar un producto disponible en el inventario.');
                                          
                }
            
		$this->render('ver', array(
			'model' => $model_venta,
		));
	}

	public function actionCrear() {
		$model = new Venta;
                $model->fecha=$model->fecha=date("Y-m-d H:i:m");
                $model->estado="NUEVA";
                $model->total=0;
                $model->user_id=Yii::app()->user->getId();
		
                
		if ($model->validate()) {
			if ($model->save()) {
                            Yii::app()->getUser()->setFlash('success','<i class="icon-ok-sign"></i>  Compra registrada exitósamente');
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		//$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Venta');

		$this->performAjaxValidation($model, 'venta-form');

		if (isset($_POST['Venta'])) {
			$model->setAttributes($_POST['Venta']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Venta')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Venta('search');
		$model->unsetAttributes();

		if (isset($_GET['Venta'])){
			$model->setAttributes($_GET['Venta']);
                }

                $session['Venta_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Venta_model_search']))
               {
                $model = $session['Venta_model_search'];
                $model = Venta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Venta::model()->findAll();
             $this->toExcel($model, array('id', 'fecha', 'total', 'estado', 'user'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Venta_model_search']))
               {
                $model = $session['Venta_model_search'];
                $model = Venta::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Venta::model()->findAll();
             $this->toExcel($model, array('id', 'fecha', 'total', 'estado', 'user'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Venta'),
		));
	}

	public function actionCreate() {
		$model = new Venta;

		$this->performAjaxValidation($model, 'venta-form');

		if (isset($_POST['Venta'])) {
			$model->setAttributes($_POST['Venta']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Venta');

		$this->performAjaxValidation($model, 'venta-form');

		if (isset($_POST['Venta'])) {
			$model->setAttributes($_POST['Venta']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Venta')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('Venta');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Venta('search');
		$model->unsetAttributes();

		if (isset($_GET['Venta'])){
			$model->setAttributes($_GET['Venta']);
                }

                $session['Venta_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}