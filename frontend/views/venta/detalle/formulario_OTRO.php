    <?php
    $model_detalles=new CArrayDataProvider($model,array( 'pagination'=>false));
    ?>

<h2>Detalle</h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'stock-grid',
	'dataProvider' => $model_detalles,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",     
	'columns' => Stock::getColumns(array('buttons'=>array('template'=>'{delete}','deleteButtonUrl'=>'Yii::app()->createUrl("/stock/borrar", array("id"=>$data->id))',), 'id'=>array('header'=>'ID'),'codigo_barra'=>array('header'=>'Codigo barra'),'fecha_creacion'=>array('header'=>'Fecha Creacion'),'estado'=>array('header'=>'Estado'))),
)); ?>