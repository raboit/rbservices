<?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_VERTICAL,null,'post',array('id' => 'stock-form',)); ?>
    <fieldset>
        <legend>Ingresar Producto</legend>
        <?php echo TbHtml::label('Escanear Producto', 'texto'); ?>
        <?php echo TbHtml::textField('codigo_barra', '',array('id' =>'codigo_barra', 'maxlength'=>36)); ?><br>
        <?php echo TbHtml::submitButton('Añadir a la venta',array('value'=>true, 'name'=>'agregar', 'color' => TbHtml::BUTTON_COLOR_SUCCESS)); ?>
        <?php echo TbHtml::submitButton('Finalizar venta',array('value'=>true,'name'=>'terminar', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
    </fieldset>
<?php echo TbHtml::endForm(); ?>

    <?php Yii::app()->clientScript->registerScript('focus','$(function(){$("#codigo_barra").focus();});',CclientScript::POS_READY);?>

    <?php
    $model_detalles=new CArrayDataProvider($model,array( 'pagination'=>false));
    ?>

<h2>Detalle</h2>
<?php 

?>
<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'stock-grid',
	'dataProvider' => $model_detalles,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",     
	'columns' => Stock::getColumns(array('id'=>array('header'=>'ID'),'codigo_barra'=>array('header'=>'Codigo barra'),'fecha_creacion'=>array('header'=>'Fecha Creacion'),'estado'=>array('header'=>'Estado'))),
)); 
?>