<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'fecha',
		'estado',
                array(
                    'name' => 'total',
                    'type' => 'raw',
                    'value' => Yii::app()->format->formatNumber($model->calcularTotal()),
                ),
//		array(
//			'name' => 'user',
//			'type' => 'raw',
//			'value' => $model->user !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->user)), array('user/ver', 'id' => GxActiveRecord::extractPkValue($model->user, true))) : null,
//			),
	),
)); ?>

<?php $model->getMenu();?>


<?php
if($model->estado=="NUEVA"):
   $this->renderPartial('/venta/detalle/formulario_NUEVA', array(
                    'model' => $model->stocks,
                    'buttons' => 'create'));
else:
    $this->renderPartial('/venta/detalle/formulario_OTRO', array(
                    'model' => $model->stocks,
                    'buttons' => 'create'));
endif;

?>