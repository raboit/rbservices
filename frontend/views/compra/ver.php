<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'fecha',
		'precio',
		'cantidad_productos',
		'estado',
		array(
			'name' => 'producto',
			'type' => 'raw',
			'value' => $model->producto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->producto)), array('producto/ver', 'id' => GxActiveRecord::extractPkValue($model->producto, true))) : null,
			),
		array(
			'name' => 'proveedor',
			'type' => 'raw',
			'value' => $model->proveedor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proveedor)), array('proveedor/ver', 'id' => GxActiveRecord::extractPkValue($model->proveedor, true))) : null,
			),
	),
)); ?>

<?php echo TbHtml::beginFormTb(TbHtml::FORM_LAYOUT_VERTICAL,null,'post',array('id' => 'stock-form',));
?>
    <fieldset>
        <legend>Ingresar Detalle</legend>
        <?php echo TbHtml::label('Escanear Producto', 'texto'); ?>
        <?php echo TbHtml::textField('codigo_barra', '',array('id' =>'codigo_barra', 'maxlength'=>36)); ?><br>
        <?php echo TbHtml::submitButton('Añadir a la compra',array('value'=>true, 'name'=>'agregar', 'color' => TbHtml::BUTTON_COLOR_SUCCESS)); ?>
    </fieldset>
<?php echo TbHtml::endForm(); ?>
<?php Yii::app()->clientScript->registerScript('focus','$(function(){$("#codigo_barra").focus();});',CclientScript::POS_READY);?>


<h2><?php echo GxHtml::encode($model->getRelationLabel('Stocks')); ?></h2>
<?php
	$this->widget('yiiwheels.widgets.grid.WhGridView', array(
	'id' => 'detalle-producto-grid',
	'dataProvider' => new CArrayDataProvider($model->stocks,array()),
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}{extendedSummary}", 
	'columns' => Stock::getColumns(array('codigo_barra'=>array('header'=>'Codigo Barra'),'fecha_creacion'=>array('header'=>'Fecha Creacion'),'compra_id'=>array('visible'=>false))),
            'extendedSummary' => array(
                'title' => 'Total',
                'columns' => array(
                    'total' => array(
                            'label'=>'Total',
                            'class'=>'yiiwheels.widgets.grid.operations.WhSumOperation'
                    ),
                )
            ),            
        'extendedSummaryOptions' => array(
            'class' => 'well pull-right',
            'style' => 'width:300px'
	),
)); ?>