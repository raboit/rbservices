<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('ver', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
        <?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('precio')); ?>:
        <?php echo GxHtml::encode($data->precio); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidad_productos')); ?>:
        <?php echo GxHtml::encode($data->cantidad_productos); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estado')); ?>:
        <?php echo GxHtml::encode($data->estado); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('producto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->producto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proveedor)); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->user)); ?>
	<br />
	*/ ?>

</div>
</a>